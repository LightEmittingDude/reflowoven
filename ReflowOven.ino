/*
 * This program controlls a reflow oven using a solid state relay and
 * a MAX31855 thermocouple amplifier breakout from Adafruit. Program 
 * is written for an Arduino Micro
 * 
 * Make the following pin connections:
 * Micro          |     MAX31855
 * -----------------------------
 * pin 33 (MISO)  |     DO
 * pin 34 (SCK)   |     CLK
 * pin 9 (D4)     |     CS
 * 
 * Author: Kasun Somaratne
 * Last modified: Jan 3, 2015
 */

#include <SPI.h>  

#define ID            "REFLOW" // this ID allows user interface to detect controller
#define SS_PIN        4
#define OVEN_PIN      7
#define SPI_FREQ      5000000 // SPI bit rate
#define HOT_TEMP_RES  0.25 // hot junction temp resolution (deg C)
#define COLD_TEMP_RES 0.0625 // cold junction temp resolution (deg C)
#define READ_DELAY    100 // temp read interval
#define HYSTERESIS    0.5 // oven on/off hysteresis (deg C)

// variables to store latest data from MAX31855
float hotJncTemp = 0.0;
float coldJncTemp = 0.0;
float targetTemp = 0.0;
float rampRate = 0.0;
int faultCode = 0;

// these two arrays store the reflow profile as time,temperature pairs
int timeArray[ 5 ]; // time values
float tempArray[ 5 ]; // temperature values

// variables to store current status
String profileName = "NO PROFILE";
int curProfStep = 0;
unsigned long profStartTime = 0;
float profStartTemp = 0.0;
unsigned long profTime = 0;
boolean serialConnected = false;
boolean runProfile = false;
boolean ovenOn = false;

//////////////////////////////////////////////////////////////////////
// setup() - Initializes communication with PC and MAX31855 and
//           configure pins
// Arguments: none
// Returns: none
//////////////////////////////////////////////////////////////////////
void setup()
{
  // initialize communication with PC (user interface)
  Serial.begin( 9600 );
  Serial.setTimeout( 1 );
  SPI.begin(); // initialize communication with MAX31855

  // configure pins and initial status
  pinMode( SS_PIN, OUTPUT );
  pinMode( OVEN_PIN, OUTPUT );
  digitalWrite( SS_PIN, HIGH );
  turnOffOven();
}

//////////////////////////////////////////////////////////////////////
// loop() - main loop of program. Processes commands from PC. Executes
//          a reflow profile through all five stages 
// Arguments: none
// Returns: none
//////////////////////////////////////////////////////////////////////
void loop()
{
  // check for any messages from PC
  if( Serial.available() > 0)
  {
    String command = Serial.readString(); // read message from PC
    if( command == "ID" ) // check if ID is requested
    {
      Serial.println( ID );
      serialConnected = true;
    }
    else if( command == "NODATA" ) // check if stop data command 
    {
      serialConnected = false;
    }
    else if( command == "START" ) // check for start profile command
    {
      // if no profile is currently stored send no profile message
      if( profileName == "NO PROFILE" )
      {
        Serial.println( profileName + ":" );
      }
      // if profile is stored, start the profile
      else
      {
        runProfile = true;
        profStartTime = millis();
        profStartTemp = hotJncTemp;
        // calculate starting temp ramp rate
        rampRate = ( tempArray[ 0 ] - profStartTemp ) / ( 1.0 * timeArray[ 0 ] ); 
        curProfStep = 0;
        // send message back to PC to indicate start of profile and finish time
        Serial.println( "START:" + profileName + ":" + String( timeArray[ 4 ] ) );
      }
    }
    else if( command == "STOP" ) // check for stop profile command
    {
      runProfile = false;
      turnOffOven();
    }
    else if( command == "UPLOAD" ) // check for upload new profile command
    {
      while( Serial.available() == 0 ){}
      profileName = Serial.readString();
      for(int i=0; i<5; i++)
      {
        while( Serial.available() == 0 ){}
        timeArray[ i ] = Serial.parseInt();
        while( Serial.available() == 0 ){}
        tempArray[ i ] = Serial.parseInt();
      }
      Serial.println( "UPLOAD DONE:" );
    }
    else if( command == "GETPROFDATA" ) // check for get profile data command
    {
      if( profileName == "NO PROFILE" )
      {
        Serial.println( profileName + ":" );
      }
      else
      {
        String profDataStr = "PROFDATA:" + profileName + ":";
        for( int i=0; i<5; i++ )
        {
          profDataStr += String( timeArray[ i ] ) + ":" + String( tempArray[ i ] ) + ":";
        }
        Serial.println( profDataStr );
      }
    }
  }
  readTemp(); // read temperature values from MAX31855
  if( runProfile )
  {
    profTime = millis() - profStartTime; // calculate current profile run time
    if( ( profTime / 1000.0 ) > timeArray[ curProfStep ] ) // if profile run time is greater than step time
    {
      if( curProfStep == 4 ) // if this is the last profile terminate profile
      {
        turnOffOven();
        runProfile = false;
        Serial.println( "PROFILE DONE:" );
      }
      else // move on to the next profile
      {
        curProfStep++; 
        // recalculate temperature ramp rate
        rampRate = ( tempArray[ curProfStep ] - tempArray[ curProfStep - 1 ] ) / ( 1.0 * ( timeArray[ curProfStep ] - timeArray[ curProfStep - 1 ] ) );
      }
    }
    if( runProfile )
    {
      calcTargetTemp(); 
      // if target temperature is less than current oven temperature then turn off oven
      if( targetTemp < hotJncTemp )
      {
        turnOffOven();
      }
      // if current temperature is less than current oven temp plus the hysteresis, turn on the oven
      else if( targetTemp >= hotJncTemp + HYSTERESIS )
      {
        turnOnOven();
      }
    }
  }
  // if a PC is connected send current oven/profile data 
  if( serialConnected )
  {
    Serial.println( String( hotJncTemp ) + ":" + String( coldJncTemp ) + ":" + String( faultCode ) + ":" + String( targetTemp ) + ":" + String( ovenOn ) + ":" + String( curProfStep ) + ":" + String( profTime ) + ":" );
  }
  //wait a little bit before reading the next sample
  delay(READ_DELAY);
}

//////////////////////////////////////////////////////////////////////
// readTemp() - reads temperature data from the MAX31855
// Arguments: none
// Returns: none
//////////////////////////////////////////////////////////////////////
void readTemp()
{
  //begin a transaction
  SPI.beginTransaction( SPISettings( SPI_FREQ, MSBFIRST, SPI_MODE0 ) );
  digitalWrite( SS_PIN, LOW );

  /*
   * First we read the hot junction temperature (bits 31 to 18) which 
   * requires reading two bytes (MSB first). Then we convert the bit value to 
   * actual temperature 
   */
  uint8_t hotTempMSB = SPI.transfer( 0 );
  uint8_t hotTempLSB = SPI.transfer( 0 );
  hotJncTemp = ( ( ( hotTempMSB << 8 ) | hotTempLSB ) >> 2 ) * HOT_TEMP_RES;

  //next we read the cold junction temperature and fault condition status bits
  uint8_t coldTempMSB = SPI.transfer( 0 );
  uint8_t coldTempLSB = SPI.transfer( 0 );
  
  //now we can end the transaction since we have read one complete data sample
  digitalWrite( SS_PIN, HIGH );
  SPI.endTransaction();

  //calculate the cold junction temperature
  coldJncTemp = ( ( ( coldTempMSB << 8 ) | coldTempLSB ) >> 4 ) * COLD_TEMP_RES;

  //finally we record any fault conditions in the thermocouple
  faultCode = 0x07 & coldTempLSB; 
}

//////////////////////////////////////////////////////////////////////
// calcTargetTemp() - calculates and sets the target temperature for 
//                    the current profile run time
// Arguments: none
// Returns: none
//////////////////////////////////////////////////////////////////////
void calcTargetTemp()
{
  if( curProfStep == 0 )
  {
    targetTemp =  profStartTemp + ( profTime / 1000.0 ) * rampRate;
  }
  else
  {
    targetTemp = tempArray[ curProfStep - 1 ] + ( profTime / 1000.0 - timeArray[ curProfStep - 1 ] ) * rampRate;
  }
}

//////////////////////////////////////////////////////////////////////
// turnOnOven() - turns on the oven
// Arguments: none
// Returns: none
//////////////////////////////////////////////////////////////////////
void turnOnOven()
{
  digitalWrite( OVEN_PIN, HIGH );
  ovenOn = true;
}

//////////////////////////////////////////////////////////////////////
// turnOffOven() - turns off the oven
// Arguments: none
// Returns: none
//////////////////////////////////////////////////////////////////////
void turnOffOven()
{
  digitalWrite( OVEN_PIN, LOW );
  ovenOn = false;
}


