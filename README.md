# README #

This repository contains programs to control a reflow oven using a solid state relay and a MAX31855 thermocouple amplifier breakout from Adafruit. Program is written for an Arduino Micro

For more information on this project please go to: http://kasunsomaratne.com/2015/10/14/reflow-oven-build-part-1/

Version 1

Creator: Kasun Somaratne
Date: Jan 3, 2016