##########################################################
# Reflow Oven Manager
#
# Manages reflow profiles in the reflow oven
# Can create, edit, view, and delete profiles
# Uploads profiles to the reflow oven and starts a profile
# Shows the current status of a running profile
# Saves profile data returned from the oven in a log file
#
# Author: Kasun Somaratne
# Date: Sep 4, 2015
##########################################################

from tkinter import *
from tkinter.ttk import *
from tkinter.filedialog import askopenfilename
from matplotlib import pyplot as plt
import tkinter.messagebox
import serial as srl
import serial.tools.list_ports
import io, time, threading, os

# a reflow profile object
class Profile:

        PREHEAT, SOAK, RAMP, PEAK, COOLDOWN = range( 0, 5 )
        stepTypes = [ "Preheat", "Soak", "Ramp", "Peak", "Cool down" ]

        def __init__( self, name ):
                self.name = name
                self.times = [0, 0, 0, 0, 0]
                self.temps = [0, 0, 0, 0, 0]
        
# a thread object that can be killed
class StoppableThread( threading.Thread ):

        def __init__( self, function ):
                threading.Thread.__init__( self )
                self.function = function
                self.stop = False

        def run( self ):
                """run the thread until stop is called"""
                while not self.stop:
                        self.function()
                        
class Application( Frame ):
        
        def __init__( self, master=None ):
                Frame.__init__( self, master )
                self.pack( side="top", fill="both", expand=True )
                self.master.protocol( 'WM_DELETE_WINDOW', self.mainWindowOnClose )
                self.ovenConnected = False
                self.editProfileMode = False
                self.profileRunning = False
                self.profiles = []
                self.loadProfileData()
                self.createMainWindow()
                self.updateProfileMenu()

        def createMainWindow( self ):
                """creates the main application window"""
                connectFrame = LabelFrame( self, text="Connect", relief="groove" )
                connectFrame.pack( side="top", padx=5, pady=5, ipady=5 )

                ## reflow oven port selection
                Label( connectFrame, text="Reflow Oven port: " ).pack( side="left", padx=10 )

                self.portVar = StringVar()
                self.portVar.set( "select" )
                self.portMenu = OptionMenu( connectFrame, self.portVar )
                self.addPortMenuItems()
                self.portMenu.pack( side="left", padx=10 )

                self.portButVar = StringVar()
                self.portButVar.set( "Connect" )
                self.portButton = Button( connectFrame, textvariable=self.portButVar, command=self.connectToOven, state="disabled" )
                self.portButton.pack( side="right", padx=10 )

                ## reflow profiles
                profileFrame = LabelFrame( self, text="Profiles", relief="groove" )
                profileFrame.pack( side="top", padx=5, pady=5, ipady=5 )

                self.createProfBut = Button( profileFrame, text="create new profile", command=self.showProfileWindow )
                self.createProfBut.pack( side="top", padx=5, pady=5 )

                # select profile menu
                self.profileMenuFrame = Frame( profileFrame, relief="groove" )
                self.profileMenuFrame.pack( side="top" )
                
                Label( self.profileMenuFrame, text="Select profile: " ).pack(side="left", padx=5, pady=5 )
                self.profileVar = StringVar()
                self.profileVar.set( "-Profile-" )
                self.profileMenu = OptionMenu( self.profileMenuFrame, self.profileVar )
                self.profileMenu.pack(side="left", padx=5, pady=5 )

                # select profile buttons
                self.profileButFrame = Frame( profileFrame )
                self.profileButFrame.pack( side="top" )
                
                self.viewProfBut = Button( self.profileButFrame, text="view profile", command=self.viewProfile, state="disabled" )
                self.viewProfBut.pack( side="left", padx=5, pady=5 )

                self.editProfBut = Button( self.profileButFrame, text="edit profile", command=self.editProfile, state="disabled" )
                self.editProfBut.pack( side="left", padx=5, pady=5 )

                self.uploadProfBut = Button( self.profileButFrame, text="upload profile", command=self.uploadProfile, state="disabled" )
                self.uploadProfBut.pack( side="left", padx=5, pady=5 )

                self.delProfBut = Button( self.profileButFrame, text="delete profile", command=self.deleteProfile, state="disabled" )
                self.delProfBut.pack( side="left", padx=5, pady=5 )

                ## Oven status
                self.ovenFrame = LabelFrame( self, text="Oven status", relief="groove" )
                self.ovenFrame.pack( side="top", padx=5, pady=5, ipady=5 )

                self.viewOvenProfBut = Button( self.ovenFrame, text="view current profile", command=self.getOvenProfile, state="disabled" )
                self.viewOvenProfBut.grid( row=0, column=0, padx=5, pady=5 )

                self.startProfButVar = StringVar()
                self.startProfButVar.set( "start profile" )
                self.startProfBut = Button( self.ovenFrame, textvariable=self.startProfButVar, command=self.startStopProfile, state="disabled" )
                self.startProfBut.grid( row=0, column=1, padx=5, pady=5 )

                Label( self.ovenFrame, text="heating: " ).grid( row=1, column=0, padx=5, pady=5, sticky="E" )
                self.heatStatus = Label( self.ovenFrame )
                self.heatOnImg = PhotoImage( file="HeatingOn.gif" )
                self.heatOffImg = PhotoImage( file="HeatingOff.gif" )
                self.heatStatus.grid( row=1, column=1, padx=5, pady=5, sticky="W" )

                Label( self.ovenFrame, text="current profile: " ).grid( row=2, column=0, padx=5, pady=5, sticky="E" )
                self.curProfile = Label( self.ovenFrame )
                self.curProfile.grid( row=2, column=1, padx=5, pady=5, sticky="W" )

                Label( self.ovenFrame, text="current step: " ).grid( row=3, column=0, padx=5, pady=5, sticky="E" )
                self.curStep = Label( self.ovenFrame )
                self.curStep.grid( row=3, column=1, padx=5, pady=5, sticky="W" )

                Label( self.ovenFrame, text="time remaining: " ).grid( row=4, column=0, padx=5, pady=5, sticky="E" )
                self.timeRemain = Label( self.ovenFrame )
                self.timeRemain.grid( row=4, column=1, padx=5, pady=5, sticky="W" )

                Label( self.ovenFrame, text="current temp (\u00b0C): " ).grid( row=5, column=0, padx=5, pady=5, sticky="E" )
                self.curTemp = Label( self.ovenFrame )
                self.curTemp.grid( row=5, column=1, padx=5, pady=5, sticky="W" )

                Label( self.ovenFrame, text="target temp (\u00b0C): " ).grid( row=6, column=0, padx=5, pady=5, sticky="E" )
                self.tarTemp = Label( self.ovenFrame )
                self.tarTemp.grid( row=6, column=1, padx=5, pady=5, sticky="W" )

                Label( self.ovenFrame, text="cold junction temp (\u00b0C): " ).grid( row=7, column=0, padx=5, pady=5, sticky="E" )
                self.coldJncTemp = Label( self.ovenFrame )
                self.coldJncTemp.grid( row=7, column=1, padx=5, pady=5, sticky="W" )

                self.disableChildren( self.ovenFrame )

                ## support commands
                supportButFrame = Frame( self )
                supportButFrame.pack( side="top" )
                
                plotLogBut = Button( supportButFrame, text="plot log data", command=self.plotLogData )
                plotLogBut.pack( side="left", padx=10, pady=10 )

                closeWindowBut = Button( supportButFrame, text="close window", command=self.mainWindowOnClose )
                closeWindowBut.pack( side="left", padx=10, pady=10 )

        def updateProfileMenu( self ):
                """updates the profile menu with a list of current profiles"""
                self.profileMenu[ "menu" ].delete(0, END)
                self.profileVar.set( "-Profile-" )
                for profile in self.profiles:
                        self.profileMenu[ "menu" ].add_command( label=profile.name, command=lambda name=profile.name : ( self.profileVar.set( name ), \
                                                                                                                         self.enableProfButtons() ) )
                self.disableChildren( self.profileButFrame )
                
        def enableProfButtons( self ):
                """enables the profile buttons"""
                self.enableChildren( self.profileButFrame )
                if not self.ovenConnected:
                        self.uploadProfBut.config( state="disabled" )
                if self.startProfButVar.get() == "stop profile":
                        self.uploadProfBut.config( state="disabled" )

        def addPortMenuItems( self ):
                """updates the port selection menu with a list of available com ports"""
                ports = list( serial.tools.list_ports.comports() )
                for itm in ports:
                    self.portMenu[ "menu" ].add_command( label=itm[ 0 ], command=lambda value=itm[ 0 ]: ( self.portVar.set( value ),
                                                                                          self.portButton.config( state="normal" ) ) )

        def connectToOven( self ):
                """connects to the chamber using selected port or disconnects from the chamber if connected"""
                if self.portButVar.get() == "Connect":
                        try:
                                self.ovenPort = srl.Serial()
                                self.ovenPort.port = self.portVar.get()
                                self.ovenPort.baudrate = 9600
                                self.ovenPort.timeout = 0.1
                                self.ovenPort.open()
                                self.ovenSio = io.TextIOWrapper( io.BufferedReader( self.ovenPort ) )

                                #confirm it is the chamber
                                self.ovenPort.write( "ID".encode() )
                                time.sleep( 0.1 )
                                response = self.ovenSio.readline().split( '\n' )
                                if response[0] == "REFLOW":
                                        self.ovenConnected = True
                                        self.enableChildren( self.ovenFrame )
                                        if not self.profileVar.get() == "-Profile-":
                                                self.uploadProfBut.config( state="normal" )
                                        self.dataThread = StoppableThread( self.processOvenData )
                                        self.dataThread.daemon = True
                                        self.dataThread.start()
                                        self.portButVar.set( "Disconnect" )
                                        self.portMenu.config( state="disabled" )
                                else:
                                        self.ovenPort.close()
                                        self.ovenConnected = False
                                        tkinter.messagebox.showerror( "Unable to connect", "That is not the reflow oven. Pick another port" )
                        except srl.serialutil.SerialException as excpt:
                                tkinter.messagebox.showerror( "Unable to connect", excpt.args[0] )
                else:
                        self.ovenPort.write( "NODATA".encode() )
                        self.dataThread.stop = True
                        self.ovenPort.close()
                        self.ovenConnected = False
                        self.heatStatus.config( image="" )
                        self.curTemp.config( text="" )
                        self.coldJncTemp.config( text="" )
                        self.disableChildren( self.ovenFrame )
                        self.uploadProfBut.config( state="disabled" )
                        self.portMenu.config( state="normal" )
                        self.portButVar.set( "Connect" )

        def showProfileWindow( self ):
                """shows a seperate window for creating or editing a profile"""
                self.disableChildren( self.profileMenuFrame )
                self.disableChildren( self.profileButFrame )
                
                self.profileWindow = Toplevel( self )
                self.profileWindow.wm_title( "Reflow Profile" )
                self.profileWindow.resizable( width=False, height=False )
                self.profileWindow.protocol( 'WM_DELETE_WINDOW', self.profileWindowOnClose )

                reflowImg = PhotoImage( file="ReflowProfileExample.gif" )
                reflowImgLabel = Label( self.profileWindow, image=reflowImg )
                reflowImgLabel.image = reflowImg #keeping a reference to the image so it doesn't go blank
                reflowImgLabel.pack( side="left", padx=5, pady=5 )

                profEntryFrame = Frame( self.profileWindow )
                profEntryFrame.pack( side="left" )

                Label( profEntryFrame, text="Profile name: " ).grid( row=0, column=0, padx=5, pady=5, sticky="W" )
                self.nameEntryVar = StringVar()
                Entry( profEntryFrame, textvariable=self.nameEntryVar, width=15 ).grid( row=0, column=1, padx=5, pady=5, sticky="W" )

                Separator( profEntryFrame, orient=HORIZONTAL ).grid( row=1, column=0, columnspan=2, sticky="EW" )

                # profile variables
                self.preTempVar = IntVar()      # max preheat temperature
                self.preTimeVar = IntVar()      # time from start temp to max preheat temp
                self.soakTempVar = IntVar()     # max soak tempearture
                self.soakTimeVar = IntVar()     # time from max preheat temp to max soak temp
                self.peakTempVar = IntVar()     # peak reflow temperature
                self.rampTimeVar = IntVar()     # time from max soak temp to peak reflow temp
                self.peakTimeVar = IntVar()     # peak reflow temperature duration
                self.coolTempVar = IntVar()     # max cooldown temperature
                self.coolTimeVar = IntVar()     # time from peak reflow temp to max cooldown temp

                PROF_VARS = ( \
                                ( "Preheat max temp (\u00b0C) : ", self.preTempVar, "Preheat duration (s) : ", self.preTimeVar ),
                                ( "Soak max temp (\u00b0C) : ", self.soakTempVar, "Soak duration (s) : ", self.soakTimeVar ),
                                ( "Peak temp (\u00b0C) : ", self.peakTempVar, "Ramp duration (s) : ", self.rampTimeVar, "Peak duration (s): ", self.peakTimeVar ),
                                ( "Cool down max temp (\u00b0C) : ", self.coolTempVar, "Cool down duration (s) : ", self.coolTimeVar ),
                            )
                row = 2
                for v in PROF_VARS:
                       Label( profEntryFrame, text=v[0] ).grid( row=row, column=0, padx=5, pady=5, sticky="W" )
                       Spinbox( profEntryFrame, from_=25, to=260, width=6, wrap=True, justify="center", textvariable=v[1] ).grid( \
                                row=row, column=1, padx=5, pady=5, sticky="W" )
                       row += 1
                       
                       Label( profEntryFrame, text=v[2] ).grid( row=row, column=0, padx=5, pady=5, sticky="W" )
                       Spinbox( profEntryFrame, from_=1, to=600, width=6, wrap=True, justify="center", textvariable=v[3] ).grid( \
                                row=row, column=1, padx=5, pady=5, sticky="W" )
                       row += 1

                       if len( v ) == 6:
                               Label( profEntryFrame, text=v[4] ).grid( row=row, column=0, padx=5, pady=5, sticky="W" )
                               Spinbox( profEntryFrame, from_=1, to=600, width=6, wrap=True, justify="center", textvariable=v[5] ).grid( \
                                        row=row, column=1, padx=5, pady=5, sticky="W" )
                               row += 1    
                       
                       Separator( profEntryFrame, orient=HORIZONTAL ).grid( row=row, column=0, columnspan=2, sticky="EW" )
                       row += 1

                processButLabel = "add profile"
                if self.editProfileMode:
                        processButLabel = "edit profile"

                self.profAddButton = Button( profEntryFrame, text=processButLabel, command=self.processProfileInfo )
                self.profAddButton.grid( row=row, column=0, columnspan=2, padx=10, pady=10 )

        def processProfileInfo( self ):
                """processes user input in the profile window"""
                proceed = False
                try:
                        # check profile name
                        if not re.match( "^[ ]+$", self.nameEntryVar.get() ) == None:
                             tkinter.messagebox.showerror( "Invalid profile name", "Please enter a valid profile name", parent=self.profileWindow )    
                        elif re.match( "^[A-Za-z0-9 ]+$", self.nameEntryVar.get() ) == None:
                             tkinter.messagebox.showerror( "Invalid profile name", "Please enter a valid profile name", parent=self.profileWindow )
                        # check profile vars
                        elif self.preTempVar.get() < 25 or self.preTempVar.get() > 260:
                                tkinter.messagebox.showerror( "Invalid range", "Please enter a valid preheat max temperature (25\u00b0C to 260\u00b0C)", parent=self.profileWindow )
                        elif self.preTimeVar.get() < 0 or self.preTimeVar.get() > 600:
                                tkinter.messagebox.showerror( "Invalid range", "Please enter a valid preheat time (0s to 600s)", parent=self.profileWindow )
                        elif self.soakTempVar.get() < 25 or self.soakTempVar.get() > 260:
                                tkinter.messagebox.showerror( "Invalid range", "Please enter a valid soak max temperature (25\u00b0C to 260\u00b0C)", parent=self.profileWindow )
                        elif self.soakTimeVar.get() < 0 or self.soakTimeVar.get() > 600:
                                tkinter.messagebox.showerror( "Invalid range", "Please enter a valid soak time (0s to 600s)", parent=self.profileWindow )
                        elif self.peakTempVar.get() < 25 or self.peakTempVar.get() > 260:
                                tkinter.messagebox.showerror( "Invalid range", "Please enter a valid reflow max temperature (25\u00b0C to 260\u00b0C)", parent=self.profileWindow )
                        elif self.rampTimeVar.get() < 0 or self.rampTimeVar.get() > 600:
                                tkinter.messagebox.showerror( "Invalid range", "Please enter a valid reflow time (0s to 600s)", parent=self.profileWindow )
                        elif self.peakTimeVar.get() < 0 or self.peakTimeVar.get() > 600:
                                tkinter.messagebox.showerror( "Invalid range", "Please enter a valid reflow time (0s to 600s)", parent=self.profileWindow )
                        elif self.coolTempVar.get() < 25 or self.coolTempVar.get() > 260:
                                tkinter.messagebox.showerror( "Invalid range", "Please enter a valid cool down max temperature (25\u00b0C to 260\u00b0C)", parent=self.profileWindow )
                        elif self.coolTimeVar.get() < 0 or self.coolTimeVar.get() > 600:
                                tkinter.messagebox.showerror( "Invalid range", "Please enter a valid cool down time (0s to 600s)", parent=self.profileWindow )
                        else:
                                proceed = True 

                except ValueError:
                        tkinter.messagebox.showerror( "Invalid data", "Please check your numbers", parent=self.profileWindow )

                if proceed:
                        newProfile = Profile( self.nameEntryVar.get() )
                        newProfile.temps[ Profile.PREHEAT ] = self.preTempVar.get()
                        newProfile.times[ Profile.PREHEAT ] = self.preTimeVar.get()
                        newProfile.temps[ Profile.SOAK ] = self.soakTempVar.get()
                        newProfile.times[ Profile.SOAK ] = newProfile.times[ Profile.PREHEAT ] + self.soakTimeVar.get()
                        newProfile.temps[ Profile.RAMP ] = self.peakTempVar.get()
                        newProfile.times[ Profile.RAMP ] = newProfile.times[ Profile.SOAK ] + self.rampTimeVar.get()
                        newProfile.temps[ Profile.PEAK ] = self.peakTempVar.get()
                        newProfile.times[ Profile.PEAK ] = newProfile.times[ Profile.RAMP ] + self.peakTimeVar.get()
                        newProfile.temps[ Profile.COOLDOWN ] = self.coolTempVar.get()
                        newProfile.times[ Profile.COOLDOWN ] = newProfile.times[ Profile.PEAK ] + self.coolTimeVar.get()

                        if self.editProfileMode:
                                self.profiles.pop( self.editIndex )
                                self.profiles.insert( self.editIndex, newProfile )
                                self.editProfileMode = False
                        else:
                                self.profiles.append( newProfile )
                        self.updateProfileMenu()
                        self.profileWindow.withdraw()
                        self.enableChildren( self.profileMenuFrame )

        def viewProfile( self ):
                """plots the selected profile"""
                xData = []
                yData = []
                profName = ""
                labels = [ "Preheat", "Soak", "Ramp", "Peak", "Cool down" ]
                colors = [ "green", "yellow", "orange", "red", "blue" ]
                
                for profile in self.profiles:
                        if profile.name == self.profileVar.get():
                                profName = profile.name
                                xData = profile.times
                                yData = profile.temps
                                break

                plt.cla()
                plt.plot( [ 0, xData[ 0 ] ], [ 25, yData[ 0 ] ], label=labels[ 0 ], color=colors[ 0 ], linewidth=3 )
                for i in range( 1, len( xData ) ):
                        plt.plot( [ xData[ i-1 ], xData[ i ] ], [ yData[ i-1 ], yData[ i ] ], label=labels[ i ], color=colors[ i ], linewidth=3 )
                
                plt.xlabel( "Time (s)" )
                plt.ylabel( "Temperature (\u00b0C)" )
                plt.title( profName + " Reflow profile" )
                plt.ylim( 20, max( yData ) + 5 )
                plt.legend()
                plt.show( block=False )

        def editProfile( self ):
                """edit the selected profile"""
                self.editProfileMode = True
                for i in range( 0, len( self.profiles ) ):
                        if self.profiles[ i ].name == self.profileVar.get():
                                self.editIndex = i
                                self.showProfileWindow()
                                self.nameEntryVar.set( self.profiles[ i ].name )

                                self.preTempVar.set( self.profiles[ i ].temps[ 0 ] )
                                self.preTimeVar.set( self.profiles[ i ].times[ 0 ] )
                                self.soakTempVar.set( self.profiles[ i ].temps[ 1 ] )
                                self.soakTimeVar.set( self.profiles[ i ].times[ 1 ] - self.profiles[ i ].times[ 0 ] )
                                self.peakTempVar.set( self.profiles[ i ].temps[ 2 ] )
                                self.rampTimeVar.set( self.profiles[ i ].times[ 2 ] - self.profiles[ i ].times[ 1 ] )
                                self.peakTimeVar.set( self.profiles[ i ].times[ 3 ] - self.profiles[ i ].times[ 2 ] )
                                self.coolTempVar.set( self.profiles[ i ].temps[ 4 ] )
                                self.coolTimeVar.set( self.profiles[ i ].times[ 4 ] - self.profiles[ i ].times[ 3 ] )
                                break

        def deleteProfile( self ):
                """deletes a selected profile"""
                for profile in self.profiles:
                        if profile.name == self.profileVar.get():
                                profName = profile.name 
                                self.profiles.remove( profile )
                                self.updateProfileMenu()
                                tkinter.messagebox.showinfo( "Profile deleted", "Profile " + profName + " successfully deleted" )
                                break
                        
        def saveProfileData( self ):
                """Saves profile data in a text file"""
                profileFile = open( "profiles.txt", 'w' )
                for profile in self.profiles:
                        profileFile.write( "Name:" + profile.name + "\n" )
                        #profileFile.write( "Times:" )
                        for time in profile.times:
                                profileFile.write( str( time ) + "," )
                        profileFile.write( "\n" )
                        #profileFile.write( "Temps:" )
                        for temp in profile.temps:
                             profileFile.write( str( temp ) + "," )
                        profileFile.write( "\n" )   

        def loadProfileData( self ):
                """loads the previously saved profiles into the program"""
                if not os.path.isfile( "profiles.txt" ):
                        return
                
                profileFile = open( "profiles.txt", 'r' )
                for line in profileFile:
                        dataList = line.split( ":" ) 
                        if dataList[ 0 ] == "Name":
                                newProfile = Profile( dataList[ 1 ].split( "\n" )[ 0 ] )
                                
                                times = profileFile.readline().split( "," ) 
                                newProfile.times[ Profile.PREHEAT ] = int( times[ 0 ] )
                                newProfile.times[ Profile.SOAK ] = int( times[ 1 ] )
                                newProfile.times[ Profile.RAMP ] = int( times[ 2 ] )
                                newProfile.times[ Profile.PEAK ] = int( times[ 3 ] )
                                newProfile.times[ Profile.COOLDOWN ] = int( times[ 4 ] )

                                temps = profileFile.readline().split( "," ) 
                                newProfile.temps[ Profile.PREHEAT ] = int( temps[ 0 ] )
                                newProfile.temps[ Profile.SOAK ] = int( temps[ 1 ] )
                                newProfile.temps[ Profile.RAMP ] = int( temps[ 2 ] )
                                newProfile.temps[ Profile.PEAK ] = int( temps[ 3 ] )
                                newProfile.temps[ Profile.COOLDOWN ] = int( temps[ 4 ] )

                                self.profiles.append( newProfile )

        def uploadProfile( self ):
                """uploads the selected profile to the reflow oven"""
                for profile in self.profiles:
                        if profile.name == self.profileVar.get():
                                self.ovenPort.write( "UPLOAD".encode() )
                                time.sleep( 0.1 )
                                self.ovenPort.write( profile.name.encode() )
                                time.sleep( 0.1 )
                                for i in range( 0, 5 ):
                                        self.ovenPort.write( str( profile.times[ i ] ).encode() )
                                        time.sleep( 0.01 )
                                        self.ovenPort.write( str( profile.temps[ i ] ).encode() )
                                        time.sleep(0.01)
                                break

        def startStopProfile( self ):
                """Starts the current profile in the reflow oven or stops the current profile if running"""
                if self.startProfButVar.get() == "start profile":
                        self.ovenPort.write( "START".encode() )
                        # open log file
                        if not os.path.isdir( "reflowData" ):
                                os.mkdir( "reflowData" )

                        fname = "_".join( ( time.asctime() ).split( ":" ) ) + ".txt"
                        self.dataLogFile = open( "reflowData/"+fname, 'w' )

                        self.viewOvenProfBut.config( state="disabled" )
                        self.uploadProfBut.config( state="disabled" )
                        self.portButton.config( state="disabled" )
                        self.startProfButVar.set( "stop profile" )

                else:
                      self.ovenPort.write( "STOP".encode() )
                      self.profileRunning = False
                      self.dataLogFile.close()
                      self.curProfile.config( text="" )
                      self.tarTemp.config( text="" )
                      self.curStep.config( text="" )
                      self.timeRemain.config( text="" )
                      self.viewOvenProfBut.config( state="normal" )
                      self.portButton.config( state="normal" )
                      if not self.profileVar.get() == "-Profile-":
                              self.uploadProfBut.config( state="normal" )
                      self.startProfButVar.set( "start profile" )

        def getOvenProfile( self ):
                """retrieves and plots the current oven profile"""
                self.ovenPort.write( "GETPROFDATA".encode() )
                time.sleep( 1.0 ) # wait for oven profile data to be sent 

                try:
                        dataList = self.ovenProfViewData
                        profName = dataList[ 1 ]
                        labels = [ "Preheat", "Soak", "Ramp", "Peak", "Cool down" ]
                        colors = [ "green", "yellow", "orange", "red", "blue" ]

                        plt.cla()
                        plt.plot( [ 0, int( float( dataList[ 2 ] ) ) ], [ 25, int( float( dataList[ 3 ] ) ) ], label=labels[ 0 ], color=colors[ 0 ], linewidth=3 )
                        for i in range( 0, 4 ):
                                plt.plot( [ int( float( dataList[ 2*i+2 ] ) ), int( float( dataList[ 2*(i+2) ] ) ) ], \
                                          [ int( float( dataList[ 2*i+3 ] ) ), int( float( dataList[ 2*(i+1)+3 ] ) ) ], label=labels[ i+1 ], color=colors[ i+1 ], linewidth=3 )

                        plt.xlabel( "Time (s)" )
                        plt.ylabel( "Temperature (\u00b0C)" )
                        plt.title( " Reflow Oven profile: " + profName )
                        plt.ylim( 20, int( float( dataList[ 9 ] ) ) + 5 )
                        plt.legend()
                        plt.show( block=False )
                        self.ovenProfViewData = []
                except ( AttributeError, IndexError ):
                        plt.close()
                        tkinter.messagebox.showerror( "Communication error", "Oven did not respond in a timely manner. Please try again!" )

        def plotLogData( self ):
                """plots the reflow log data from the selected file"""
                logDataFname = askopenfilename()
                logDataFile = open( logDataFname, 'r' )
                timeData = []
                tempData = []
                tarTempData = []

                for line in logDataFile:
                        dataList = line.split( ":" )
                        timeData.append( int( dataList[ 6 ] ) / 1000.0 )
                        tempData.append( float( dataList[ 0 ] ) )
                        tarTempData.append( float( dataList[ 3 ] ) )

                logDataFile.close() 
                plt.plot( timeData, tempData, label="Oven temp" )
                plt.plot( timeData, tarTempData, label="Target temp" )

                plt.xlabel( "Time (s)" )
                plt.ylabel( "Temperature (\u00b0C)" )
                plt.title( " Reflow profile temperature as a function of time ")
                plt.legend()
                plt.show( block=False )
                   
        def profileWindowOnClose( self ):
                """calls when the profile window is closed"""
                self.profileWindow.destroy()
                self.enableChildren( self.profileMenuFrame )

        def processOvenData( self ):
                """processes the incoming data from the Oven"""
                inData = self.ovenSio.readline()
                if len( inData ) > 0:
                        dataList = inData.split( ":" )
                        if dataList[0] == "UPLOAD DONE":
                                tkinter.messagebox.showinfo( "Profile uploaded", "Profile successfully uploaded!" )
                        elif dataList[0] == "PROFILE DONE":
                                self.profileRunning = False
                                self.dataLogFile.close()
                                self.portButton.config( state="normal" )
                                self.viewOvenProfBut.config( state="normal" )
                                if not self.profileVar.get() == "-Profile-":
                                        self.uploadProfBut.config( state="normal" )
                                self.startProfButVar.set( "start profile" )
                                tkinter.messagebox.showinfo( "Profile complete", "The profile has completed!" )
                                self.curProfile.config( text="" )
                                self.tarTemp.config( text="" )
                                self.curStep.config( text="" )
                                self.timeRemain.config( text="" )
                        elif dataList[0] == "NO PROFILE":
                                tkinter.messagebox.showinfo( "No profile", "Please upload a profile to the reflow oven first" )
                                self.portButton.config( state="normal" )
                                self.viewOvenProfBut.config( state="normal" )
                                if not self.profileVar.get() == "-Profile-":
                                        self.uploadProfBut.config( state="normal" )
                                self.startProfButVar.set( "start profile" )
                        elif dataList[0] == "START":
                                self.profileRunning = True
                                self.curProfile.config( text=dataList[1] )
                                self.curProfDuration = int( float( dataList[2] ) )
                        elif dataList[0] == "PROFDATA":
                                self.ovenProfViewData = dataList
                        elif len( dataList ) == 8:
                                self.curTemp.config( text=dataList[ 0 ] )
                                self.coldJncTemp.config( text=dataList[ 1 ] )
                                if int( dataList[ 4 ] ) == 1:
                                        self.heatStatus.config( image=self.heatOnImg )
                                        self.heatStatus.image = self.heatOnImg
                                else:
                                        self.heatStatus.config( image=self.heatOffImg )
                                        self.heatStatus.image = self.heatOffImg
                                if self.profileRunning:
                                        self.dataLogFile.write( inData )
                                        self.tarTemp.config( text=dataList[ 3 ] )
                                        self.curStep.config( text=Profile.stepTypes[ int( dataList[ 5 ] ) ] )
                                        timeRemain = int( self.curProfDuration - int( dataList[6] ) / 1000 )
                                        if timeRemain > 59:
                                                timeRemainStr = str( int( timeRemain / 60 ) ) + "m " + str( int( timeRemain % 60 ) ) + "s"
                                                self.timeRemain.config( text=timeRemainStr )
                                        else:
                                                timeRemainStr = str( int( timeRemain ) ) + "s " 
                                                self.timeRemain.config( text=timeRemainStr )

        def disableChildren( self, frame ):
                """disables all child items of a given frame"""
                for child in frame.winfo_children():
                    child.config( state = "disabled" )

        def enableChildren( self, frame ):
                """enables all child items of a given frame"""
                for child in frame.winfo_children():
                    child.config( state = "normal" )

        def mainWindowOnClose( self ):
                """function to call when the main window closes"""
                self.saveProfileData()
                if self.ovenConnected:
                        self.ovenPort.write( "NODATA".encode() )
                        self.dataThread.stop = True
                        self.ovenPort.close()
                self.quit()
                        
root = Tk()
root.resizable( width=False, height=False )
app = Application( master=root )
app.master.title( "Reflow Oven Manager" )
app.mainloop()
try:
    root.destroy()
except TclError:
    pass
